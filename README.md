# micronaut-jinja

Provides a Micronaut view renderer using Jinjava.

## Example build.gradle

```
plugins {
    id "java"
}

repositories {
    mavenCentral()
    maven {
        url 'https://gitlab.com/api/v4/projects/15155042/packages/maven'
    }
}

dependencies {
    implementation group: "is.wolf", name: "micronaut-jinja", version: "1.0-SNAPSHOT"
    implementation group: "com.hubspot.jinjava", name: "jinjava", version: "2.5.2"
    implementation group: "io.micronaut", name: "micronaut-views", version: "1.2.0"
}
```

## Example application.yml

```
micronaut:                                                                                                                                                                                                                                                    
    views:                                                                                                                                                                                                                                                    
        jinja:                                                                                                                                                                                                                                                
            enabled: true 
```
