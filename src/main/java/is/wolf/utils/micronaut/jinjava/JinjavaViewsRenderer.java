package is.wolf.utils.micronaut.jinjava;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hubspot.jinjava.Jinjava;
import com.hubspot.jinjava.interpret.RenderResult;
import io.micronaut.context.annotation.Requires;
import io.micronaut.core.io.ResourceLoader;
import io.micronaut.core.io.Writable;
import io.micronaut.core.util.ArgumentUtils;
import io.micronaut.core.util.StringUtils;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Produces;
import io.micronaut.views.ViewsConfiguration;
import io.micronaut.views.ViewsRenderer;
import io.micronaut.views.exceptions.ViewRenderingException;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Singleton;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@RequiredArgsConstructor
@Requires(property= JinjavaViewsRendererConfigurationProperties.PREFIX + ".enabled", notEquals = StringUtils.FALSE)
@Produces(MediaType.TEXT_HTML)
@Requires(classes= Jinjava.class)
@Singleton
public class JinjavaViewsRenderer implements ViewsRenderer {
    private final ObjectMapper objectMapper;
    private final JinjavaViewsRendererConfiguration JinjavaViewsRendererConfiguration;
    private final ViewsConfiguration viewsConfiguration;
    private final JinjavaMicronautResourceLocator resourceLocator;
    private final Jinjava jinjava;

    protected TypeReference<Map<String, Object>> typeRef = new TypeReference<Map<String, Object>>() {};

    @Nonnull
    @Override
    public Writable render(@Nonnull String viewName, @Nullable Object data) {
        ArgumentUtils.requireNonNull("viewName", viewName);
        return (writer) -> {
            try {
                String templateText = resourceLocator.getString(viewName).get();
                Map<String, Object> mappedData;
                try {
                    mappedData = (Map<String, Object>) data;
                } catch (ClassCastException exc) {
                    mappedData = objectMapper.convertValue(data, typeRef);
                }
                RenderResult result = jinjava.renderForResult(templateText, mappedData);
                if (result.hasErrors()) {
                    throw new JinjavaViewRenderingException(viewName, result.getErrors());
                } else {
                    writer.write(result.getOutput());
                }
            } catch (JinjavaViewRenderingException e){
                throw e;
            } catch (Throwable e) {
                throw new ViewRenderingException("Error rendering Jinja view [" + viewName + "]: " + e.getMessage(), e);
            }
        };
    }

    @Override
    public boolean exists(@Nonnull String viewName) {
        try {
            return resourceLocator.getString(viewName).isPresent();
        } catch(IOException exc){
            return false;
        }
    }
}
