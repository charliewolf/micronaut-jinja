package is.wolf.utils.micronaut.jinjava;

import com.hubspot.jinjava.JinjavaConfig;
import io.micronaut.context.annotation.Factory;

import javax.inject.Singleton;

@Factory
public class JinjavaConfigFactory {
    @Singleton
    public JinjavaConfig jinjavaConfig(JinjavaViewsRendererConfiguration config){
        return JinjavaConfig.newBuilder().withFailOnUnknownTokens(config.isStrict()).build();
    }
}
