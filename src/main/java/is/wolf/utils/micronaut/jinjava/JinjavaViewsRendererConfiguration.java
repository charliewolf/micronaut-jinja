package is.wolf.utils.micronaut.jinjava;

import io.micronaut.core.util.Toggleable;

public interface JinjavaViewsRendererConfiguration extends Toggleable {
    /**
     * @return Default extension for templates
     */
    String getDefaultExtension();

    boolean isStrict();
}
