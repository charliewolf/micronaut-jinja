package is.wolf.utils.micronaut.jinjava;

import com.hubspot.jinjava.Jinjava;
import com.hubspot.jinjava.JinjavaConfig;
import com.hubspot.jinjava.interpret.Context;
import com.hubspot.jinjava.lib.filter.Filter;
import com.hubspot.jinjava.lib.tag.Tag;
import io.micronaut.context.ApplicationContext;
import io.micronaut.context.annotation.Factory;

import javax.inject.Inject;
import javax.inject.Singleton;

@Factory
public class JinjavaFactory {
    @Singleton
    public Jinjava jinjava(JinjavaConfig config, JinjavaMicronautResourceLocator resourceLocator, ApplicationContext appContext){
        Jinjava jinjava = new Jinjava(config);
        jinjava.setResourceLocator(resourceLocator);
        Context context = jinjava.getGlobalContext();
        appContext.getBeansOfType(Filter.class).forEach(context::registerFilter);
        appContext.getBeansOfType(Tag.class).forEach(context::registerTag);
        return jinjava;
    }
}
