package is.wolf.utils.micronaut.jinjava;

import com.hubspot.jinjava.interpret.JinjavaInterpreter;
import com.hubspot.jinjava.loader.LocationResolver;
import com.hubspot.jinjava.loader.ResourceLocator;
import com.hubspot.jinjava.loader.ResourceNotFoundException;
import io.micronaut.core.io.ResourceLoader;
import io.micronaut.core.io.ResourceResolver;
import io.micronaut.core.io.scan.ClassPathResourceLoader;
import io.micronaut.views.ViewUtils;
import io.micronaut.views.ViewsConfiguration;
import io.micronaut.views.ViewsRenderer;
import org.apache.commons.io.IOUtils;

import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

@Singleton
public class JinjavaMicronautResourceLocator implements ResourceLocator {
    private final JinjavaViewsRendererConfiguration jinjavaViewsRendererConfiguration;
     private final ViewsConfiguration viewsConfiguration;
     private final ResourceLoader resourceLoader;

    protected JinjavaMicronautResourceLocator(JinjavaViewsRendererConfiguration config, ViewsConfiguration viewsConfiguration){
        this.jinjavaViewsRendererConfiguration = config;
        this.viewsConfiguration = viewsConfiguration;
        this.resourceLoader = new ResourceResolver().getLoader(ClassPathResourceLoader.class).get();
    }

    protected String viewLocation(final String name) {
        return viewsConfiguration.getFolder() + ViewUtils.normalizeFile(name, extension()) +  ViewsRenderer.EXTENSION_SEPARATOR + extension();
    }

    protected String extension() {
        return jinjavaViewsRendererConfiguration.getDefaultExtension();
    }

    public Optional<String> getString(String fullName, Charset encoding) throws IOException {
        Optional<InputStream> stream = resourceLoader.getResourceAsStream(viewLocation(fullName));
        if(stream.isPresent()) {
            return Optional.ofNullable(IOUtils.toString(stream.get(), encoding));
        } else {
            return Optional.empty();
        }
    }

    public Optional<String> getString(String fullName) throws IOException {
        return getString(fullName, StandardCharsets.UTF_8);
    }

    @Override
    public String getString(String fullName, Charset encoding, JinjavaInterpreter interpreter) throws IOException {
        return this.getString(fullName, encoding).orElseThrow(() -> new ResourceNotFoundException("Couldn't find resource: " + fullName));
    }
}
