@Configuration
@Requires(property = JinjavaViewsRendererConfigurationProperties.PREFIX + ".enabled", notEquals = StringUtils.FALSE)
@Requires(classes=Jinjava.class)
package is.wolf.utils.micronaut.jinjava;

import com.hubspot.jinjava.Jinjava;
import io.micronaut.context.annotation.Configuration;
import io.micronaut.context.annotation.Requires;
import io.micronaut.core.util.StringUtils;