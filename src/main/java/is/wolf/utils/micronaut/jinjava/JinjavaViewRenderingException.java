package is.wolf.utils.micronaut.jinjava;

import com.hubspot.jinjava.interpret.TemplateError;
import io.micronaut.views.exceptions.ViewRenderingException;
import lombok.Getter;

import java.util.List;

public class JinjavaViewRenderingException extends ViewRenderingException {
    @Getter
    private final List<TemplateError> templateErrors;

    JinjavaViewRenderingException(String templateName, List<TemplateError> templateErrors){
        super("Error rendering Jinja view [" + templateName + "]");
        this.templateErrors = templateErrors;
    }
}
