package is.wolf.utils.micronaut.jinjava;

import io.micronaut.context.annotation.ConfigurationProperties;
import io.micronaut.core.util.StringUtils;
import io.micronaut.views.ViewsConfigurationProperties;
import lombok.Getter;
import lombok.Setter;


@ConfigurationProperties(JinjavaViewsRendererConfigurationProperties.PREFIX)
public class JinjavaViewsRendererConfigurationProperties implements JinjavaViewsRendererConfiguration {
    @SuppressWarnings("WeakerAccess")
    public static final String PREFIX = ViewsConfigurationProperties.PREFIX + ".jinja";

    @SuppressWarnings("WeakerAccess")
    public static final String DEFAULT_EXTENSION = "j2";

    /**
     * The default enable value.
     */
    @SuppressWarnings("WeakerAccess")
    public static final boolean DEFAULT_ENABLED = true;

    public static final boolean DEFAULT_STRICT = true;

    @Setter @Getter
    private boolean enabled = DEFAULT_ENABLED;

    @Getter @Setter
    private boolean strict = DEFAULT_STRICT;

    @Getter
    private String defaultExtension = DEFAULT_EXTENSION;

    /**
     * The default extension. Default value ({@value #DEFAULT_EXTENSION}).
     *
     * @param defaultExtension The extension
     */
    public void setDefaultExtension(String defaultExtension) {
        if (StringUtils.isNotEmpty(defaultExtension)) {
            this.defaultExtension = defaultExtension;
        }
    }
}
