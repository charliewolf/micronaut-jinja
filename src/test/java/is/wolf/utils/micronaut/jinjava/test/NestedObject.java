package is.wolf.utils.micronaut.jinjava.test;

import lombok.Data;

@Data
public class NestedObject {
    private final Person person;
}
