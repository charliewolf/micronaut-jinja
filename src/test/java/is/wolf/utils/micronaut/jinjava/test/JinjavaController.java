package is.wolf.utils.micronaut.jinjava.test;

import io.micronaut.core.util.CollectionUtils;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.views.View;


@Controller("/jinja")
public class JinjavaController {
    @Get("/")
    @View("home")
    public HttpResponse index() {
        return HttpResponse.ok(CollectionUtils.mapOf("loggedIn", true, "username", "cwolf"));
    }

    @Get("/pojo")
    @View("home")
    public HttpResponse<Person> pojo() {
        return HttpResponse.ok(new Person("cwolf", true));
    }
    @Get("/filter")
    @View("custom_filter")
    public HttpResponse<Person> customFilter() {
        return HttpResponse.ok(new Person("cwolf", true));
    }

    @Get("/tag")
    @View("custom_tag")
    public HttpResponse<Person> customTag() {
        return HttpResponse.ok(new Person("cwolf", true));
    }

    @Get("/nested")
    @View("nested")
    public HttpResponse<NestedObject> nested() {
        return HttpResponse.ok(new NestedObject(new Person("cwolf", true)));
    }

    @Get("/home")
    public HttpResponse<Person> home() {
        return HttpResponse.ok(new Person("cwolf", true));
    }

    @Get("/bogus")
    @View("bogus")
    public HttpResponse<Person> bogus() {
        return HttpResponse.ok(new Person("cwolf", true));
    }

    @View("home")
    @Get("/nullbody")
    HttpResponse nullBody() {
        return HttpResponse.ok();
    }

    @View("missing_var")
    @Get("/missing")
    HttpResponse missingVar() {
        return HttpResponse.ok();
    }
}
