package is.wolf.utils.micronaut.jinjava.test;

import lombok.Data;

@Data
public class Person {
    private final String username;
    private final boolean loggedIn;
}
