package is.wolf.utils.micronaut.jinjava.test;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@MicronautTest(environments = {"test-relaxed"})
public class JinjavaTestRelaxed {
    @Inject
    private EmbeddedServer server; //refers to the server that was started up for this test suite

    private RxHttpClient httpClient;

    @BeforeEach
    public void createHttpServer(){
        this.httpClient = server.getApplicationContext().createBean(RxHttpClient.class, server.getURL());
    }

    @Test
    @DisplayName("missing variable doesn't throw error when strict mode is off")
    void testMissingVar(){
        HttpResponse<String> resp = httpClient.toBlocking().exchange("/jinja/missing", String.class);
        assertEquals(resp.getStatus(), HttpStatus.OK);
        assertTrue(resp.body().contains("<span></span>"));
        assertTrue(resp.getContentType().isPresent());
        assertEquals(resp.getContentType().get(), MediaType.TEXT_HTML_TYPE);
    }

}
