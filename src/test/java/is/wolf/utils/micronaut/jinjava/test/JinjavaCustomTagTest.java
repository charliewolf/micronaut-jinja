package is.wolf.utils.micronaut.jinjava.test;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@MicronautTest(environments = {"test", "test-tag"})
public class JinjavaCustomTagTest {    @Inject
private EmbeddedServer server; //refers to the server that was started up for this test suite

    private RxHttpClient httpClient;

    @BeforeEach
    public void createHttpServer(){
        this.httpClient = server.getApplicationContext().createBean(RxHttpClient.class, server.getURL());
    }

    @Test
    @DisplayName("test custom tag")
    void testCustomTag(){
        HttpResponse<String> resp = httpClient.toBlocking().exchange("/jinja/tag", String.class);
        assertEquals(resp.getStatus(), HttpStatus.OK);
        assertTrue(resp.body().contains("This is a snippet"));
        assertTrue(resp.getContentType().isPresent());
        assertEquals(resp.getContentType().get(), MediaType.TEXT_HTML_TYPE);
    }

}
