package is.wolf.utils.micronaut.jinjava.test;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.annotation.MicronautTest;
import io.micronaut.views.ViewsFilter;
import is.wolf.utils.micronaut.jinjava.JinjavaViewsRenderer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static  org.junit.jupiter.api.Assertions.*;


import javax.inject.Inject;


@MicronautTest(environments = {"test"})
public class JinjavaTest {
    @Inject
    private EmbeddedServer server; //refers to the server that was started up for this test suite

    private RxHttpClient httpClient;

    @BeforeEach
    public void createHttpServer(){
        this.httpClient = server.getApplicationContext().createBean(RxHttpClient.class, server.getURL());
    }

    @Test
    @DisplayName("beans are loadable")
    void testBeanIsLoaded(){
        server.getApplicationContext().getBean(ViewsFilter.class);
        server.getApplicationContext().getBean(JinjavaViewsRenderer.class);
    }

    @Test
    @DisplayName("endpoint not annotated with View returns JSON")
    void testNoViewAnnotation(){
        HttpResponse<String> resp = httpClient.toBlocking().exchange("/jinja/home", String.class);
        assertEquals(resp.getStatus(), HttpStatus.OK);
        assertTrue(resp.body().contains("{\"username\":\"cwolf\",\"loggedIn\":true}"));
        assertTrue(resp.getContentType().isPresent());
        assertEquals(resp.getContentType().get(), MediaType.APPLICATION_JSON_TYPE);
    }

    @Test
    @DisplayName("render Map")
    void testMap(){
        HttpResponse<String> resp = httpClient.toBlocking().exchange("/jinja", String.class);
        assertEquals(resp.getStatus(), HttpStatus.OK);
        assertTrue(resp.body().contains("<h1>username: <span>cwolf</span></h1>"));
        assertTrue(resp.getContentType().isPresent());
        assertEquals(resp.getContentType().get(), MediaType.TEXT_HTML_TYPE);
    }

    @Test
    @DisplayName("render POJO")
    void testPojo(){
        HttpResponse<String> resp = httpClient.toBlocking().exchange("/jinja/pojo", String.class);
        assertEquals(resp.getStatus(), HttpStatus.OK);
        assertTrue(resp.body().contains("<h1>username: <span>cwolf</span></h1>"));
        assertTrue(resp.getContentType().isPresent());
        assertEquals(resp.getContentType().get(), MediaType.TEXT_HTML_TYPE);
    }

    @Test
    @DisplayName("render nested POJO")
    void testNestedPojo(){
        HttpResponse<String> resp = httpClient.toBlocking().exchange("/jinja/nested", String.class);
        assertEquals(resp.getStatus(), HttpStatus.OK);
        assertTrue(resp.body().contains("<h1>username: <span>cwolf</span></h1>"));
        assertTrue(resp.getContentType().isPresent());
        assertEquals(resp.getContentType().get(), MediaType.TEXT_HTML_TYPE);
    }

    @Test
    @DisplayName("render with null body")
    void testNullBody(){
        HttpResponse<String> resp = httpClient.toBlocking().exchange("/jinja/nullbody", String.class);
        assertEquals(resp.getStatus(), HttpStatus.OK);
        assertTrue(resp.body().contains("<h1>You are not logged in</h1>"));
        assertTrue(resp.getContentType().isPresent());
        assertEquals(resp.getContentType().get(), MediaType.TEXT_HTML_TYPE);
    }

    @Test
    @DisplayName("custom filter that isn't loaded fails")
    void testCustomFilter() {
        assertThrows(HttpClientResponseException.class, () -> httpClient.toBlocking().exchange("/jinja/filter", String.class));
    }

    @Test
    @DisplayName("custom tag that isn't loaded fails")
    void testCustomTag() {
        assertThrows(HttpClientResponseException.class, () -> httpClient.toBlocking().exchange("/jinja/tag", String.class));
    }

    @Test
    @DisplayName("template that doesnt exist returns 404")
    void testBogus(){
        try {
            httpClient.toBlocking().exchange("/jinja/bogus", String.class);
            fail();
        } catch(HttpClientResponseException exc){
            assertEquals(exc.getResponse().getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    @DisplayName("missing variable causes error in strict mode")
    void testMissingVar(){
        assertThrows(HttpClientResponseException.class, () -> httpClient.toBlocking().exchange("/jinja/missing", String.class));
    }
}
