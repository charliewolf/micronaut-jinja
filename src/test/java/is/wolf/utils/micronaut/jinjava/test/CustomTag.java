package is.wolf.utils.micronaut.jinjava.test;

import com.hubspot.jinjava.lib.tag.IncludeTag;
import io.micronaut.context.annotation.Requires;

import javax.inject.Singleton;

@Singleton
@Requires(env = "test-tag")
public class CustomTag extends IncludeTag {
    @Override
    public String getName() {
        return "custom_include";
    }
}
