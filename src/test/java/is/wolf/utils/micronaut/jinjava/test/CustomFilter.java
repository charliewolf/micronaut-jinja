package is.wolf.utils.micronaut.jinjava.test;

import com.hubspot.jinjava.interpret.JinjavaInterpreter;
import com.hubspot.jinjava.lib.filter.Filter;
import io.micronaut.context.annotation.Requires;

import javax.inject.Singleton;

@Singleton
@Requires(env = "test-filter")
public class CustomFilter implements Filter {
    @Override
    public Object filter(Object var, JinjavaInterpreter interpreter, String... args) {
        return var.toString().toUpperCase();
    }

    @Override
    public String getName() {
        return "test_custom_filter";
    }
}
